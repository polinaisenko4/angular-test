export interface RegisterrRequestInterface {
    user: {
        email: string
        password: string
        username: string
    }
}